<?php
// src/AppBundle/Twig/AppExtension.php

namespace AppleBlog\CatalogueBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('tailleDuTexte', array($this, 'tailleDuTexteFilter')),
        );
    }

    public function tailleDuTexteFilter($contenu, $taille=-1) {

      $length = 100; // on veut les 256 premiers caratères
      if($taille>-1)
      $length=$taille;
      if(strlen($contenu) > $length) { // si la longueur de $contenu est plus grande que $length
        $chaineCoupee = substr($contenu,0,$length).'...'; /* alors on coupe $contenu à partir du début (0) jusqu'à $length (soit 256) et tout ce qui vient ensuite sera remplacé par "..." */
        return $chaineCoupee;
      }
      return $contenu;
    }



    public function getName()
    {
        return 'app_extension';
    }
}
