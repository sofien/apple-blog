<?php

namespace AppleBlog\CatalogueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="AppleBlog\CatalogueBundle\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Produit", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $produit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="urlphoto", type="string", length=255, nullable=true)
     */
    private $urlphoto;

    /**
     * @var string
     *
     * @ORM\Column(name="texte", type="text")
     */
    private $texte;

    /**
    * @var ArrayCollection
    *
    * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="article", cascade={"persist", "remove", "merge"})
    * @ORM\JoinColumn(nullable=true)
    */
    private $commentaires;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Article
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set urlphoto
     *
     * @param string $urlphoto
     *
     * @return Article
     */
    public function setUrlphoto($urlphoto)
    {
        $this->urlphoto = $urlphoto;

        return $this;
    }

    /**
     * Get urlphoto
     *
     * @return string
     */
    public function getUrlphoto()
    {
        return $this->urlphoto;
    }

    /**
     * Set texte
     *
     * @param string $texte
     *
     * @return Article
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * Get texte
     *
     * @return string
     */
    public function getTexte()
    {
        return $this->texte;
    }

    public function __construct() {
        $this->commentaires = new ArrayCollection();
        $this->date = new \Datetime();

    }

    /***********************************/
    private $brochure;

    public function getBrochure()
    {
        return $this->brochure;
    }

    public function setBrochure($brochure)
    {
        $this->brochure = $brochure;

        return $this;
    }
    /***********************************/

    private $file;

    public function getFile(){
        return $this->file;
    }

    public function setFile(UploadedFile $file = null){
        $this->file = $file;
    }

    public function upload(){
        if (null === $this->file) {
            return;
        }
        // On récupère le nom original du fichier de l'internaute
        $name = $this->file->getClientOriginalName();
        // On déplace le fichier envoyé dans le répertoire de notre choix
        $this->file->move($this->getUploadRootDir(), $name);
        // On sauvegarde le nom de fichier dans notre attribut $url
        $this->urlphoto = $this->getUploadDir().'/'.$name;
        // On crée également le futur attribut alt de notre balise <img>
        //$this->alt = $name;
      }

      public function getUploadDir()
      {
        // On retourne le chemin relatif vers l'image pour un navigateur (relatif au répertoire /web donc)
        return 'bundles/images';
      }

      protected function getUploadRootDir()
      {
        // On retourne le chemin relatif vers l'image pour notre code PHP
        //return "/Users/S-Setsuna-F/Documents/Master2/e_application/BlogProject/apple_blog/web/bundles/images/";
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
      }

    /**
     * Set produit
     *
     * @param \AppleBlog\CatalogueBundle\Entity\Produit $produit
     *
     * @return Article
     */
    public function setProduit(\AppleBlog\CatalogueBundle\Entity\Produit $produit)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \AppleBlog\CatalogueBundle\Entity\Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Add commentaire
     *
     * @param \AppleBlog\CatalogueBundle\Entity\Commentaire $commentaire
     *
     * @return Article
     */
    public function addCommentaire(\AppleBlog\CatalogueBundle\Entity\Commentaire $commentaire)
    {
        $this->commentaires[] = $commentaire;

        return $this;
    }

    /**
     * Remove commentaire
     *
     * @param \AppleBlog\CatalogueBundle\Entity\Commentaire $commentaire
     */
    public function removeCommentaire(\AppleBlog\CatalogueBundle\Entity\Commentaire $commentaire)
    {
        $this->commentaires->removeElement($commentaire);
    }

    /**
     * Get commentaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * Get commentaires_size
     *
     * @return int
     */
    public function getCommentairesSize()
    {
        return $this->commentaires->count();
    }
}
