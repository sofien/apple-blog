<?php

namespace AppleBlog\CatalogueBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CommentaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('pseudo', TextType::class,     array( 'label'=> 'Nom/Pseudo:',         'required' => true, 'attr' => array( 'class' => 'w3-input', 'placeholder' => 'pseudo ou nom', 'data_class' => null)))
                ->add('mail',   TextType::class,     array( 'label' => 'Email:',             'required' => true, 'attr' => array( 'class' => 'w3-input', 'placeholder' => 'email', 'data_class' => null)))
                ->add('comm',   TextAreaType::class, array( 'label' => 'Votre commentaire:', 'required' => true, 'attr' => array( 'class' => 'w3-input', 'placeholder' => 'votre texte', )))
                ;
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppleBlog\CatalogueBundle\Entity\Commentaire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appleblog_cataloguebundle_commentaire';
    }


}
