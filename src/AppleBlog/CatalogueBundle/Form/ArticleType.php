<?php

namespace AppleBlog\CatalogueBundle\Form;

use Symfony\Component\Form\AbstractType;
use AppleBlog\CatalogueBundle\Entity\Produit;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
//use KMS\FroalaEditorBundle\Form\Type\FroalaEditorType;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('titre',      TextType::class,   array( 'required' => true, 'attr' => array( 'class' => 'form-control', 'placeholder' => 'titre')))
                ->add('file',       FileType::class,   array( 'required' => true, 'label' => 'photo de l\'article', 'data_class' => null))
                ->add('produit',    ChoiceType::class, array( 'required' => true, 'choices'  => array(
                    'iPhone' => new Produit("iPhone"),
                    'iPod' => new Produit("iPod"),
                    'iPad' => new Produit("iPad"),
                    'iMac' => new Produit("iMac"),
                    /*'Watch' => new Produit("Watch"),
                    'Tv' => new Produit("Tv"),
                    'Music' => new Produit("Music"),*/
                    'Watch' => new Produit("AppleWatch"),
                    'Tv' => new Produit("AppleTv"),
                    'Music' => new Produit("AppleMusic"),
                ),))
                ->add('texte',      TextAreaType::class,  array( 'attr' => array( 'class' => 'tinymce', 'data-theme' => 'bbcode')), array('required' => true))
                //->add('texte',      FroalaEditorType::class, array('required' => true, "language" => "fr",
                        /*"toolbarInline" => true,
                        "tableColors" => [ "#FFFFFF", "#FF0000" ],
                        "saveParams" => [ "id" => "myEditorField" ]))*/
                //->add('save',       SubmitType::class,    array( 'attr' => array( 'class' => 'btn btn-primary'), 'label' => 'Editer' ))
                ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppleBlog\CatalogueBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appleblog_cataloguebundle_article';
    }


}
