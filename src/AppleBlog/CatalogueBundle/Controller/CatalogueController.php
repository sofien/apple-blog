<?php

namespace AppleBlog\CatalogueBundle\Controller;

use AppleBlog\CatalogueBundle\Entity\Article;
use AppleBlog\CatalogueBundle\Entity\Commentaire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\dateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CatalogueController extends Controller
{
    public function indexAction($produit, Request $request){
        $repository =   $this->getDoctrine()->getManager();
        if( $produit === "iPhone"
         || $produit === "iPhone"
         || $produit === "iPod"
         || $produit === "iPad"
         || $produit === "iMac"
         || $produit === "AppleWatch"
         || $produit === "AppleTv"
         || $produit === "AppleMusic"){
            $findArticles = $repository ->getRepository('AppleBlogCatalogueBundle:Article')->catalogueByProduits($produit);
        } else{
            $findArticles = $repository ->getRepository('AppleBlogCatalogueBundle:Article')->catalogue();
        }
        $paginator = $this->get('knp_paginator');
        $articles  = $paginator->paginate($findArticles, $request->query->get('page', 1)/*page number*/, 6/*limit per page*/);
        $count = count($articles);
        return $this->render('AppleBlogCatalogueBundle:Catalogue:catalogue.html.twig', array('articles' => $articles, 'nomproduit' => $produit, 'taille'=>$count));
    }

    public function articleAction($id, Request $request){
            $commentaire = new Commentaire();
            $form = $this->createForm('AppleBlog\CatalogueBundle\Form\CommentaireType', $commentaire);
            $form->handleRequest($request);
            $erreurs=null;

            $repository = $this->getDoctrine() ->getManager();
            $article = $repository->getRepository('AppleBlogCatalogueBundle:Article')->createQueryBuilder('a')
                                                                                     ->where('a.id = '.$id)
                                                                                     ->getQuery()->getResult();
         if(!$article)
            throw $this->createNotFoundException('La page n\'existe pas.');


        if ($form->isSubmitted() && $form->isValid()) {
            if(!$form->getData()->getComm() || !$form->getData()->getMail() || !$form->getData()->getPseudo()){
                $erreurs = array();
                if(!$form->getData()->getComm())
                 $erreurs[] = "commentaire";
                if(!$form->getData()->getMail())
                  $erreurs[] = "mail";
                if(!$form->getData()->getPseudo())
                  $erreurs[] = "pseudo";
            }else{
                $commentaire->setArticle($article);
                $em = $this->getDoctrine()->getManager();
                $em->persist($commentaire);
                $em->persist($article->addCommentaire($commentaire));
                $em->flush($commentaire);

                return $this->redirectToRoute('apple_blog_catalogue_article', array('id' => $article->getId()));
            }
        }

        return $this->render('AppleBlogCatalogueBundle:Catalogue:article.html.twig',
                             array( 'article'   => $article,
                                    'form'      => $form->createView(),
                                    'erreurs' => $erreurs,
                                ));
    }
}
