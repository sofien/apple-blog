<?php

namespace AppleBlog\CatalogueBundle\Controller;

use AppleBlog\CatalogueBundle\Entity\Article;
use AppleBlog\CatalogueBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Article controller.
 *
 */
class ArticleController extends Controller
{
    /**
     * Lists all article entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('AppleBlogCatalogueBundle:Article')->findAll();

        $paginator = $this->get('knp_paginator');
        $articles  = $paginator->paginate($articles, $request->query->get('page', 1)/*page number*/, 10/*limit per page*/);

        return $this->render('AppleBlogCatalogueBundle::article/index.html.twig', array(
            'articles' => $articles,
        ));
    }

    /**
     * Creates a new article entity.
     *
     */
    public function newAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm('AppleBlog\CatalogueBundle\Form\ArticleType', $article);
        $form->handleRequest($request);
        $erreurs=null;

        if ($form->isSubmitted() && $form->isValid()) {
            if(!$form->getData()->getTitre() /*|| !$form->getData()->getUrlphoto()*/ || !$form->getData()->getTexte()){
                $erreurs = array();
                if(!$form->getData()->getTitre())
                 $erreurs[] = "titre";
                if(!$form->getData()->getUrlphoto())
                  $erreurs[] = "photo";
                if(!$form->getData()->getTexte())
                  $erreurs[] = "texte";
            }else{
                $article->upload();//Methode pour sauvegarder l'image
                $em = $this->getDoctrine()->getManager();
                $em->persist($article);
                $em->persist($article->getProduit());
                $em->flush($article);

                return $this->redirectToRoute('adminArticles_show', array('id' => $article->getId()));
            }
        }
        return $this->render('AppleBlogCatalogueBundle::article/new.html.twig', array(
            'article' => $article,
            'form' => $form->createView(),
            'erreurs' => $erreurs,
        ));
    }

    /**
     * Finds and displays a article entity.
     *
     */
    public function showAction(Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);

        return $this->render('AppleBlogCatalogueBundle::article/show.html.twig', array(
            'article' => $article,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing article entity.
     *
     */
    public function editAction(Request $request, Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm('AppleBlog\CatalogueBundle\Form\ArticleType', $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $article->upload();
            $this->getDoctrine()->getManager()->flush();

            //return $this->redirectToRoute('adminArticles_edit', array('id' => $article->getId()));
            return $this->redirectToRoute('adminArticles_index');
        }

        return $this->render('AppleBlogCatalogueBundle::article/edit.html.twig', array(
            'article' => $article,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a article entity.
     *
     */
    public function deleteAction(Request $request, Article $article)
    {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush($article);
        }

        return $this->redirectToRoute('adminArticles_index');
    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param Article $article The article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adminArticles_delete', array('id' => $article->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    private function ajouterProduit(){
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('AppleBlogCatalogueBundle:Produit')->findAll();

        $vide=true;//Si on a des elements dans la BDD
        foreach ($produits as $p) {
            $vide=false;
            break;//On met vide a faut et on sort.
        }
        //Si on est dans le cas ou la BDD est vide
        //if($vide){
            $produit = new Produit();

            $produit->setNom("iPhone");
            $produit->setNom("iPod");
            $produit->setNom("iPad");
            $produit->setNom("iMac");
            $produit->setNom("&#xF8FF;Watch");
            $produit->setNom("&#xF8FF;Tv");
            $produit->setNom("&#xF8FF;Music");
            $em->persist($produit);

            $em->persist("iPhone");
            $em->persist("iPod");
            $em->persist("iPad");
            $em->persist("iMac");
            $em->persist("&#xF8FF;Watch");
            $em->persist("&#xF8FF;Tv");
            $em->persist("&#xF8FF;Music");

            $em->flush($produit);
        //}
    }

}
