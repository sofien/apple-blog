<?php
/*
namespace Utilisateurs\UtilisateurBundle\Controller;

use AppleBlog\CatalogueBundle\Entity\Article;
use Utilisateurs\UtilisateurBundle\Entity\Utilisateurs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Bundle\FrameworkBundle\dateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTime;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EditeurController extends Controller
{
    public function editeurAction(){

        $repository = $this->getDoctrine()
                           ->getManager();

        $articles = $repository->getRepository('AppleBlogCatalogueBundle:Article')->createQueryBuilder('a')
                            ->orderBy('a.date', 'ASC')
                            ->getQuery()->getResult();

        return $this->render('UtilisateurBundle:ModulesUsed:editeur.html.twig', array('articles' => $articles));
    }

    public function editerAction(Request $request){
      // On crée un objet Article
      $article = new Article();

      $form = $this->createFormBuilder($article)
      ->add('titre',      TextType::class,      array( 'attr' => array( 'class' => 'form-control', 'placeholder' => 'titre', 'required' => true)))
      ->add('urlphoto',   FileType::class,      array('label' => 'photo de l\'article', 'required' => true))
      ->add('texte',      TextAreaType::class,  array( 'attr' => array( 'class' => 'tinymce', 'data-theme' => 'bbcode')), array('required' => true))
      ->add('save',       SubmitType::class,    array( 'attr' => array( 'class' => 'btn btn-primary'), 'label' => 'Editer' ))

      //->add('titre',      TextType::class)
      //->add('urlphoto',   FileType::class,      array('label' => 'photo de l\'article'))
      //->add('texte',      TextAreaType::class,  array( 'attr' => array( 'class' => 'tinymce', 'data-theme' => 'bbcode')))
      //->add('save',       SubmitType::class,    array( 'label' => 'Editer' ))
          //->add('cancel',     SubmitType::class,    array('label' => 'Annuler'))
          ->getForm();

          $erreur=null;
          $form->handleRequest($request);

         if ($form->isValid()) {
             $article = $form->getData();
             if($article->getTitre() && $article->getUrlphoto() && $article->getTexte()){
                //$em = $this->getDoctrine()->getManager();
                //$em->persist($article);
                //$em->flush();

                //$article->bind($this->get('request'));
                //echo $article->getTitre();
                echo $article->getId();
                echo $article->getTitre();
                echo $article->getUrlphoto();
                echo $article->getTexte();

                $article->setDate(new \DateTime());
                //die();

                //return $this->redirectToRoute('apple_blog_catalogue_homepage');


                 //return $this->redirectToRoute('article_success');
                 $repository = $this->getDoctrine()->getManager();
                 //$articles   = $repository->getRepository('AppleBlogCatalogueBundle:Article')->createQueryBuilder('a')->orderBy('a.date', 'ASC')->getQuery()->getResult();
                 //return $this->render('AppleBlogCatalogueBundle:Catalogue:catalogue.html.twig', array('articles' => $article));

                 return $this->render('UtilisateurBundle:ModulesUsed:reponseFormulaire.html.twig', array('articles' => $article));
             }
             else{
               $erreur = array();
               if(!$article->getTitre())
                  $erreur[] = "titre";
               if(!$article->getUrlphoto())
                   $erreur[] = "photo";
               if(!$article->getTexte())
                   $erreur[] = "texte";
             }
       }

      return $this->render('AppleBlogCatalogueBundle:Catalogue:editer.html.twig', array(
          'form' => $form->createView(),
          'erreurs'=> $erreur,
      ));
    }

}*/
